$(document).ready(function () {
    $(`.tabs-caption li`).click(switchTabs);

    function switchTabs() {
        $(this).addClass(`active`).siblings().removeClass(`active`);
        const tabIndex = $(this).index();
        $(`.tabs-content`).removeClass(`active animated flipInY`).eq(tabIndex).addClass(`active animated flipInY`);
    }
});
$(document).ready(function () {
    $('.item').hide();
    $('.item').slice(0, 12).show();
    $('.categories-title').click(function () {
        $(this).addClass('active').siblings().removeClass('active');
        const imageType = $(this).data('filter');
        $('.item').hide();
        $(`.item${imageType}`).removeClass('animated bounceInDown').slice(0, 12).show().addClass('animated bounceInDown');
        if ($(`.item${imageType}:hidden`).length < 1) {
            $('#add-img').hide()
        } else {
            $('#add-img').show()
        }
    });
});
    $(document).ready(function () {
        $('#anim1').hide();
    $('#add-img').click(function () {
        const imageType = $('.categories-title.active').data('filter');
        $('#add-img').hide();
        $('#anim1').show();
        setTimeout(function () {
            $(`.item${imageType}:hidden`).slice(0, 12).show().addClass('animated bounceInDown');
            $('#anim1').hide();
            $('#add-img').show();
            if ($(`.item${imageType}:hidden`).length < 1) {
                $('#add-img').hide()
            }
        }, 2000);
    })

});

$(document).ready(function () {
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        asNavFor: '.slider-nav',
    });
    $('.slider-nav').slick({
        autoplay: false,
        autoplaySpeed: 7000,
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        focusOnSelect: true,
        variableWidth: true,
        prevArrow: "<i class=\"fas fa-chevron-left previous arrow-border\"></i>",
        nextArrow: "<i class=\"fas fa-chevron-right next arrow-border\"></i>",
    });
});

$(document).ready(function () {
    $('.hidden-img').hide();
    $('.hidden-img').slice(0, 8).show();
    $('#anim2').hide();
    $('#show-more').click(function () {
        $('#show-more').hide();
        $('#anim2').show();
        setTimeout(function () {
            $('.hidden-img:hidden').slice(0, 2).show().addClass('animated bounceInDown');
            $('#anim2').hide();
            $('#show-more').show();
            if ($('.hidden-img:hidden').length < 1) {
                $('#show-more').hide();
            }
            let msnry = new Masonry(elem, {
                itemSelector: '.grid-item',
                gutter: 15,
                percentPosition: true,
                isAnimated: true,
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: false
                }
            });
        }, 2000);
    });
    let elem = document.querySelector('.grid');
    let msnry = new Masonry(elem, {
        itemSelector: '.grid-item',
        // columnWidth: '.grid-sizer',
        gutter: 15,
        percentPosition: true,
        isAnimated: true,
        animationOptions: {
            duration: 750,
            easing: 'linear',
            queue: false
        }
    });
});


